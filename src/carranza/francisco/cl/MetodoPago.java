package carranza.francisco.cl;

public class MetodoPago {
    private int numero;
    private int codSeguridad;
    private String proveedor;

    public MetodoPago() {
    }

    public MetodoPago(int numero, int codSeguridad, String proveedor) {
        this.numero = numero;
        this.codSeguridad = codSeguridad;
        this.proveedor = proveedor;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public int getCodSeguridad() {
        return codSeguridad;
    }

    public void setCodSeguridad(int codSeguridad) {
        this.codSeguridad = codSeguridad;
    }

    public String getProveedor() {
        return proveedor;
    }

    public void setProveedor(String proveedor) {
        this.proveedor = proveedor;
    }

    @Override
    public String toString() {
        return "MetodoPago{" +
                "numero=" + numero +
                ", codSeguridad=" + codSeguridad +
                ", proveedor='" + proveedor + '\'' +
                '}';
    }
}
