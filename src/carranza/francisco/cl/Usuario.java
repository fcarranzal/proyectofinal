package carranza.francisco.cl;

import java.time.LocalDate;

public class Usuario extends Persona{
    private String avatar;
    private boolean genero;
    private int celular;

    public Usuario() {
    }

    public Usuario(String nombre, String apellidos, int identificacion, String direccion, String correoElectronico, String clave, LocalDate fechaNacimiento, int edad, String avatar, boolean genero, int celular) {
        super(nombre, apellidos, identificacion, direccion, correoElectronico, clave, fechaNacimiento, edad);
        this.avatar = avatar;
        this.genero = genero;
        this.celular = celular;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isGenero() {
        return genero;
    }

    public void setGenero(boolean genero) {
        this.genero = genero;
    }

    public int getCelular() {
        return celular;
    }

    public void setCelular(int celular) {
        this.celular = celular;
    }

    @Override
    public String toString() {
        return "Usuario{" +
                "avatar='" + avatar + '\'' +
                ", genero=" + genero +
                ", celular=" + celular +
                '}';
    }
}
