package carranza.francisco.cl;

import java.time.LocalDate;


public class Pedido {
    private int numero;
    private LocalDate fecha;
    private String datosCliente;
    private String detallesServicio;
    private String direccion;
    private String medioPago;
    private String estado;

    public Pedido() {
    }

    public Pedido(int numero, LocalDate fecha, String datosCliente, String detallesServicio, String direccion, String medioPago, String estado) {
        this.numero = numero;
        this.fecha = fecha;
        this.datosCliente = datosCliente;
        this.detallesServicio = detallesServicio;
        this.direccion = direccion;
        this.medioPago = medioPago;
        this.estado = estado;
    }

    public int getNumero() {
        return numero;
    }

    public void setNumero(int numero) {
        this.numero = numero;
    }

    public LocalDate getFecha() {
        return fecha;
    }

    public void setFecha(LocalDate fecha) {
        this.fecha = fecha;
    }

    public String getDatosCliente() {
        return datosCliente;
    }

    public void setDatosCliente(String datosCliente) {
        this.datosCliente = datosCliente;
    }

    public String getDetallesServicio() {
        return detallesServicio;
    }

    public void setDetallesServicio(String detallesServicio) {
        this.detallesServicio = detallesServicio;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getMedioPago() {
        return medioPago;
    }

    public void setMedioPago(String medioPago) {
        this.medioPago = medioPago;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Pedido{" +
                "numero=" + numero +
                ", fecha=" + fecha +
                ", datosCliente='" + datosCliente + '\'' +
                ", detallesServicio='" + detallesServicio + '\'' +
                ", direccion='" + direccion + '\'' +
                ", medioPago='" + medioPago + '\'' +
                ", estado='" + estado + '\'' +
                '}';
    }
}
