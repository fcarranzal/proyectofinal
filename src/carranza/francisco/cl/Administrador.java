package carranza.francisco.cl;

import java.time.LocalDate;

public class Administrador extends Persona {

    public Administrador() {
    }

    public Administrador(String nombre, String apellidos, int identificacion, String direccion, String correoElectronico, String clave, LocalDate fechaNacimiento, int edad) {
        super(nombre, apellidos, identificacion, direccion, correoElectronico, clave, fechaNacimiento, edad);
    }

}
