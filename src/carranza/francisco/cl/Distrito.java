package carranza.francisco.cl;

public class Distrito {
    private String nombre;
    private String identificador;

    public Distrito() {
    }

    public Distrito(String nombre, String identificador) {
        this.nombre = nombre;
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return "Distrito{" +
                "nombre='" + nombre + '\'' +
                ", identificador='" + identificador + '\'' +
                '}';
    }
}
