package carranza.francisco.cl;

public class Servicio {
    private int codigo;
    private String descripcion;
    private double precioVase;
    private String tipo;
    private boolean estado;

    public Servicio() {
    }

    public Servicio(int codigo, String descripcion, double precioVase, String tipo, boolean estado) {
        this.codigo = codigo;
        this.descripcion = descripcion;
        this.precioVase = precioVase;
        this.tipo = tipo;
        this.estado = estado;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public double getPrecioVase() {
        return precioVase;
    }

    public void setPrecioVase(double precioVase) {
        this.precioVase = precioVase;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Servicio{" +
                "codigo=" + codigo +
                ", descripcion='" + descripcion + '\'' +
                ", precioVase=" + precioVase +
                ", tipo='" + tipo + '\'' +
                ", estado=" + estado +
                '}';
    }
}
