package carranza.francisco.cl;

public class MedioTransporte {
    private int placa;
    private String marca;
    private String categoria;

    public MedioTransporte() {
    }

    public MedioTransporte(int placa, String marca, String categoria) {
        this.placa = placa;
        this.marca = marca;
        this.categoria = categoria;
    }

    public int getPlaca() {
        return placa;
    }

    public void setPlaca(int placa) {
        this.placa = placa;
    }

    public String getMarca() {
        return marca;
    }

    public void setMarca(String marca) {
        this.marca = marca;
    }

    public String getCategoria() {
        return categoria;
    }

    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    @Override
    public String toString() {
        return "MedioTransporte{" +
                "placa=" + placa +
                ", marca='" + marca + '\'' +
                ", categoria='" + categoria + '\'' +
                '}';
    }
}
