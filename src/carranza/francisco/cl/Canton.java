package carranza.francisco.cl;

public class Canton {
    private String nombre;
    private String identificador;

    public Canton() {
    }

    public Canton(String nombre, String identificador) {
        this.nombre = nombre;
        this.identificador = identificador;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIdentificador() {
        return identificador;
    }

    public void setIdentificador(String identificador) {
        this.identificador = identificador;
    }

    @Override
    public String toString() {
        return "Canton{" +
                "nombre='" + nombre + '\'' +
                ", identificador='" + identificador + '\'' +
                '}';
    }
}
