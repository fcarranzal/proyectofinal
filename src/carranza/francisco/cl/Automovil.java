package carranza.francisco.cl;

public class Automovil extends MedioTransporte{
    private String color;
    private String categoria;
    private String annio;

    public Automovil() {
    }

    public Automovil(int placa, String marca, String categoria, String color, String categoria1, String annio) {
        super(placa, marca, categoria);
        this.color = color;
        this.categoria = categoria1;
        this.annio = annio;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String getCategoria() {
        return categoria;
    }

    @Override
    public void setCategoria(String categoria) {
        this.categoria = categoria;
    }

    public String getAnnio() {
        return annio;
    }

    public void setAnnio(String annio) {
        this.annio = annio;
    }

    @Override
    public String toString() {
        return "Automovil{" +
                "color='" + color + '\'' +
                ", categoria='" + categoria + '\'' +
                ", annio='" + annio + '\'' +
                '}';
    }
}
