package carranza.francisco.cl;

import java.time.LocalDate;

public class Conductor extends Persona{
    private String avatar;
    private boolean estado;

    public Conductor() {
    }

    public Conductor(String nombre, String apellidos, int identificacion, String direccion, String correoElectronico, String clave, LocalDate fechaNacimiento, int edad, String avatar, boolean estado) {
        super(nombre, apellidos, identificacion, direccion, correoElectronico, clave, fechaNacimiento, edad);
        this.avatar = avatar;
        this.estado = estado;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }

    @Override
    public String toString() {
        return "Conductor{" +
                "avatar='" + avatar + '\'' +
                ", estado=" + estado +
                '}';
    }
}
