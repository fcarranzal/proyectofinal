package carranza.francisco.ui;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class UI {
    private static BufferedReader in = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        mostrarMenu();
    }
    public static void mostrarMenu() throws IOException{
        int opcion =0;
        do{
            System.out.println("****  ***");
            System.out.println("1. .");
            System.out.println("0. Salir");
            System.out.print("POr favor digite una opción: ");
            opcion = Integer.parseInt(in.readLine());
            procesarOpcion(opcion);
        }while(opcion !=0);
    }

    public static void procesarOpcion(int opcion) throws IOException{
        switch (opcion){

            case 0:
                System.out.println("Gracias por su visita");
            default:
                System.out.println("Opción invalida!");
        }
    }
}
